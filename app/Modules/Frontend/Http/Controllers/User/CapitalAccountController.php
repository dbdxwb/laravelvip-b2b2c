<?php

namespace App\Modules\Frontend\Http\Controllers\User;

use App\Modules\Base\Http\Controllers\UserCenter;
use Illuminate\Http\Request;

class CapitalAccountController extends UserCenter
{

    protected $memberCard;

    public function __construct()
    {
        parent::__construct();

    }



    public function lists(Request $request)
    {
        $seo_title = '用户中心';

        $params = $request->all();


        // 获取数据
        $user = [];

        $where = [];
        // 搜索条件

        $where[] = ['user_id', $this->user_id];

        // 列表
        $condition = [
            'where' => $where,
            'sortname' => 'id',
            'sortorder' => 'desc'
        ];

        list($list, $total) = [[], 0];

        $pageHtml = frontend_pagination($total);
        $page_array = frontend_pagination($total,true);
        $page_json = json_encode($page_array);

        $trade_type_items = [];
        $is_deposit = "1";
        $is_recharge = "1";
        $nav_default = 'capital-account';
        $erp_shop_exists = true;

        $compact = compact('seo_title','pageHtml', 'user', 'list', 'page_json',
            'trade_type_items','is_deposit','is_recharge','nav_default','erp_shop_exists');

        if ($request->ajax() && !is_app()) { // web端访问 ajax请求
            $render = view('user.capital-account.partials._list', $compact)->render();
            return result(0, $render);
        }

        $webData = []; // web端（pc、mobile）数据对象
        $data = [
            'app_prefix_data' => [
                'user' => $user,
                'page' => $page_array,
                'list' => $list,
                'trade_type_items' => $trade_type_items,
                'is_deposit' => $is_deposit,
                'is_recharge' => $is_recharge,
                'nav_default' => $nav_default,
                'erp_shop_exists' => $erp_shop_exists,
            ],
            'app_suffix_data' => [],
            'web_data' => $webData,
            'compact_data' => $compact,
            'tpl_view' => 'user.capital-account.list'
        ];
        $this->setData($data); // 设置数据
        return $this->displayData(); // 模板渲染及APP客户端返回数据
    }

    public function getData()
    {
        $result = [
            'balance' => '0.00',
            'points' => 0
        ];
        return response()->json($result);
    }

    public function view(Request $request)
    {
        $compact = [];
        $render = view('user.capital-account.view', compact($compact))->render();
        return result(0, $render);
    }
}